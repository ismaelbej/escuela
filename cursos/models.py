# -*- coding: utf-8 -*-
from django.db import models
from estudiantes.models import Estudiante
from profesores.models import Profesor


class Materia(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre


class Curso(models.Model):
    materia = models.ForeignKey(Materia)
    fecha = models.DateField()
    profesor = models.ForeignKey(Profesor, null=True, blank=True, default=None)

    def __str__(self):
        return str(self.materia) + u" - " + str(self.fecha)


class Cursada(models.Model):
    estudiante = models.ForeignKey(Estudiante)
    curso = models.ForeignKey(Curso)
    primer_parcial = models.NullBooleanField(u"primer parcial")
    segundo_parcial = models.NullBooleanField(u"segundo parcial")
    final = models.IntegerField(null=True, default=None)

    def __str__(self):
        return str(self.curso) + u" - " + str(self.estudiante)