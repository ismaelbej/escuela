# -*- coding: utf-8 -*-
from django.db import models


class Estudiante(models.Model):
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    fecha_nacimiento = models.DateField(u"fecha nacimiento")

    def __str__(self):
        return self.apellido + ", " + self.nombre
