Administración Escolar
======================

Este es un proyecto de ejemplo sobre el uso módulo Django-Admin.
La idea detrás del ejemplo es usar lo máximo posible la
interfaz pública Django-Admin.

Este ejemplo se empezó con la versión de django 1.5. Pero actualmente
esta escrito para la versión 1.7 o posterior. Esta versión incluye varios
mejoras que hacen innecesarios varios *hacks* que usaba para la versión anterior.
