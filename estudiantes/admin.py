# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Estudiante
from escuela.admin import site
from common.admin import EscuelaModelAdmin


@admin.register(Estudiante, site=site)
class EstudianteAdmin(EscuelaModelAdmin):
    list_col_actions = [(u"Reporte", 'report'), (u"Editar", 'change'), (u"Eliminar", 'delete')]
    list_display = ('apellido', 'nombre', 'col_actions')
    list_display_links = None
