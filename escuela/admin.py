from django.contrib import admin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect


class AdminSite(admin.AdminSite):
    site_header = u'Administración Escolar'

    def app_index(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('admin:index'))

site = AdminSite(name='admin')
site.disable_action('delete_selected')
