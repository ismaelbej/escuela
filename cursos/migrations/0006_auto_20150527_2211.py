# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cursos', '0005_curso_profesor'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cursada',
            name='final',
            field=models.IntegerField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='cursada',
            name='primer_parcial',
            field=models.NullBooleanField(verbose_name='primer parcial'),
        ),
        migrations.AlterField(
            model_name='cursada',
            name='segundo_parcial',
            field=models.NullBooleanField(verbose_name='segundo parcial'),
        ),
    ]
