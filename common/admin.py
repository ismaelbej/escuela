# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from escuela.utils import link_action


def get_field_value(obj, field_name):
    from django.db.models import DateField, ManyToManyField, BooleanField
    from django.db.models import NullBooleanField

    try:
        if hasattr(obj, 'get_%s_display' % (field_name,)):
            return getattr(obj, 'get_%s_display' % (field_name,))()
        elif hasattr(obj, '%s_id' % (field_name,)):
            return getattr(obj, field_name)

        field = obj._meta.get_field(field_name)
        if isinstance(field, ManyToManyField):
            return u" - ".join([str(value) for value in getattr(obj, field_name).all()])
        if isinstance(field, DateField):
            return getattr(obj, field_name).strftime('%d/%m/%Y')
        if isinstance(field, [BooleanField, NullBooleanField]):
            val = getattr(obj, field_name, None)
            if val in [True, False]:
                return u"Si" if val else u"No"
            return u""
        return getattr(obj, field_name)

    except:
        return u""


class EscuelaModelAdmin(admin.ModelAdmin):
    list_col_actions = []
    report_view_template = 'admin/report_view.html'
    report_fieldsets = []

    def col_actions(self, obj):
        return u" | ".join(self.get_col_actions(obj))
    col_actions.allow_tags = True
    col_actions.short_description = ''

    def get_col_actions(self, obj):
        return [
            link_action(
                reverse('admin:%s_%s_%s' % (self.model._meta.app_label, self.model._meta.model_name, action), args=[obj.id]),
                text)
            for (text, action) in self.list_col_actions
        ]

    def get_urls(self):
        urls = super(EscuelaModelAdmin, self).get_urls()
        return [
            url(r'^(?P<pk>\d+)/report/$',
                self.admin_site.admin_view(self.report_view),
                name='%s_%s_report' % (self.model._meta.app_label, self.model._meta.model_name)),
        ] + urls

    def get_report_fieldsets(self, request):
        if self.report_fieldsets:
            return self.report_fieldsets
        return self.get_fieldsets(request)

    def report_view(self, request, pk):
        def get_values(object, fields):
            for field_name in fields:
                field = object._meta.get_field(field_name)
                yield (field.verbose_name, get_field_value(object, field_name))

        def get_fieldsets(object, fieldsets):
            for header, fields_dict in fieldsets:
                classes = fields_dict.get('classes', [])
                yield (header, get_values(object, fields_dict['fields']), classes)

        opts = self.model._meta
        object = get_object_or_404(self.model, pk=pk)

        context = dict(self.admin_site.each_context(request),
                       object=object,
                       opts=opts,
                       title=u"Reporte %s" % (opts.verbose_name,),
                       fieldsets=get_fieldsets(object, self.get_report_fieldsets(request)),
                       has_change_permission=self.has_change_permission(request, object),
                       )
        return TemplateResponse(request, self.report_view_template, context)
