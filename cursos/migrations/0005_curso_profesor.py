# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profesores', '0001_initial'),
        ('cursos', '0004_rename_calificacion_cursada'),
    ]

    operations = [
        migrations.AddField(
            model_name='curso',
            name='profesor',
            field=models.ForeignKey(to='profesores.Profesor', default=None, blank=True, null=True),
        ),
    ]
