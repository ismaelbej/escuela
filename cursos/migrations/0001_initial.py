# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estudiantes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Curso',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('fecha', models.DateField()),
                ('estudiantes', models.ManyToManyField(to='estudiantes.Estudiante')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Materia',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Nota',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('primer_parcial', models.NullBooleanField()),
                ('segundo_parcial', models.NullBooleanField()),
                ('final', models.IntegerField()),
                ('curso', models.ForeignKey(to='cursos.Curso')),
                ('estudiante', models.ForeignKey(to='estudiantes.Estudiante')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='curso',
            name='materia',
            field=models.ForeignKey(to='cursos.Materia'),
            preserve_default=True,
        ),
    ]
