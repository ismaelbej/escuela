# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estudiantes', '0001_initial'),
        ('cursos', '0003_remove_curso_estudiantes'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='calificacion',
            new_name='cursada',
        ),
        migrations.AlterModelOptions(
            name='cursada',
            options={
            },
        ),
    ]
