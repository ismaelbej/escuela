# -*- coding: utf-8 -*-
from django.utils.safestring import mark_safe


def link_action(uri, text, classes=[]):
    return mark_safe('<a href="%s" classes="%s">%s</a>' % (uri, u" ".join(classes), text))
