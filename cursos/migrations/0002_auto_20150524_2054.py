# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estudiantes', '0001_initial'),
        ('cursos', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Nota',
            new_name='Calificacion',
        ),
        migrations.AlterModelOptions(
            name='Calificacion',
            options={
                'verbose_name_plural': 'calificaciones',
                'verbose_name': 'calificación',
            },
        ),
    ]
