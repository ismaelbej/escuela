# -*- coding: utf-8 -*-
from django.db import models


class Profesor(models.Model):
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)

    def __str__(self):
        return self.apellido + ", " + self.nombre

    class Meta:
        verbose_name = u"profesor"
        verbose_name_plural = u"profesores"
