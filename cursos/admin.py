# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Curso, Materia, Cursada
from escuela.admin import site
from common.admin import EscuelaModelAdmin


@admin.register(Curso, site=site)
class CursoAdmin(EscuelaModelAdmin):
    list_col_actions = [(u"Reporte", 'report'), (u"Editar", 'change'), (u"Eliminar", 'delete')]
    list_display = ('materia', 'fecha', 'profesor', 'col_actions')
    list_display_links = None


@admin.register(Materia, site=site)
class MateriaAdmin(EscuelaModelAdmin):
    list_col_actions = [(u"Reporte", 'report'), (u"Editar", 'change'), (u"Eliminar", 'delete')]
    list_display = ('nombre', 'col_actions')
    list_display_links = None


@admin.register(Cursada, site=site)
class CursadaAdmin(EscuelaModelAdmin):
    list_col_actions = [(u"Reporte", 'report'), (u"Editar", 'change'), (u"Eliminar", 'delete')]
    list_display = ('curso', 'estudiante', 'col_actions')
    list_display_links = None

